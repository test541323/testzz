import '/backend/api_requests/api_calls.dart';
import '/components/imageuploaded_widget.dart';
import '/components/textfield_widget.dart';
import '/flutter_flow/flutter_flow_drop_down.dart';
import '/flutter_flow/flutter_flow_icon_button.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import '/flutter_flow/flutter_flow_widgets.dart';
import '/flutter_flow/form_field_controller.dart';
import '/flutter_flow/upload_data.dart';
import '/custom_code/widgets/index.dart' as custom_widgets;
import '/flutter_flow/custom_functions.dart' as functions;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class EditequnrModel extends FlutterFlowModel {
  ///  Local state fields for this page.

  bool addclassheader = false;

  List<dynamic> classinfo = [];
  void addToClassinfo(dynamic item) => classinfo.add(item);
  void removeFromClassinfo(dynamic item) => classinfo.remove(item);
  void removeAtIndexFromClassinfo(int index) => classinfo.removeAt(index);
  void updateClassinfoAtIndex(int index, Function(dynamic) updateFn) =>
      classinfo[index] = updateFn(classinfo[index]);

  List<FFUploadedFile> uploadedfile = [];
  void addToUploadedfile(FFUploadedFile item) => uploadedfile.add(item);
  void removeFromUploadedfile(FFUploadedFile item) => uploadedfile.remove(item);
  void removeAtIndexFromUploadedfile(int index) => uploadedfile.removeAt(index);
  void updateUploadedfileAtIndex(
          int index, Function(FFUploadedFile) updateFn) =>
      uploadedfile[index] = updateFn(uploadedfile[index]);

  List<String> charcodes = [];
  void addToCharcodes(String item) => charcodes.add(item);
  void removeFromCharcodes(String item) => charcodes.remove(item);
  void removeAtIndexFromCharcodes(int index) => charcodes.removeAt(index);
  void updateCharcodesAtIndex(int index, Function(String) updateFn) =>
      charcodes[index] = updateFn(charcodes[index]);

  List<String> charvalues = [];
  void addToCharvalues(String item) => charvalues.add(item);
  void removeFromCharvalues(String item) => charvalues.remove(item);
  void removeAtIndexFromCharvalues(int index) => charvalues.removeAt(index);
  void updateCharvaluesAtIndex(int index, Function(String) updateFn) =>
      charvalues[index] = updateFn(charvalues[index]);

  List<dynamic> getClassInfo = [];
  void addToGetClassInfo(dynamic item) => getClassInfo.add(item);
  void removeFromGetClassInfo(dynamic item) => getClassInfo.remove(item);
  void removeAtIndexFromGetClassInfo(int index) => getClassInfo.removeAt(index);
  void updateGetClassInfoAtIndex(int index, Function(dynamic) updateFn) =>
      getClassInfo[index] = updateFn(getClassInfo[index]);

  String? paramtest = '';

  List<String> listvalue = [];
  void addToListvalue(String item) => listvalue.add(item);
  void removeFromListvalue(String item) => listvalue.remove(item);
  void removeAtIndexFromListvalue(int index) => listvalue.removeAt(index);
  void updateListvalueAtIndex(int index, Function(String) updateFn) =>
      listvalue[index] = updateFn(listvalue[index]);

  List<String> classheaderbyobject = [];
  void addToClassheaderbyobject(String item) => classheaderbyobject.add(item);
  void removeFromClassheaderbyobject(String item) =>
      classheaderbyobject.remove(item);
  void removeAtIndexFromClassheaderbyobject(int index) =>
      classheaderbyobject.removeAt(index);
  void updateClassheaderbyobjectAtIndex(int index, Function(String) updateFn) =>
      classheaderbyobject[index] = updateFn(classheaderbyobject[index]);

  List<dynamic> flPicker = [];
  void addToFlPicker(dynamic item) => flPicker.add(item);
  void removeFromFlPicker(dynamic item) => flPicker.remove(item);
  void removeAtIndexFromFlPicker(int index) => flPicker.removeAt(index);
  void updateFlPickerAtIndex(int index, Function(dynamic) updateFn) =>
      flPicker[index] = updateFn(flPicker[index]);

  List<dynamic> bay = [];
  void addToBay(dynamic item) => bay.add(item);
  void removeFromBay(dynamic item) => bay.remove(item);
  void removeAtIndexFromBay(int index) => bay.removeAt(index);
  void updateBayAtIndex(int index, Function(dynamic) updateFn) =>
      bay[index] = updateFn(bay[index]);

  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();
  // Stores action output result for [Backend Call - API (GetClassInfo)] action in editequnr widget.
  ApiCallResponse? apiResulta4x;
  // Stores action output result for [Backend Call - API (GetClassbyEqart)] action in editequnr widget.
  ApiCallResponse? apiResult0al;
  // Stores action output result for [Backend Call - API (CreateEquipmentEdit)] action in Button widget.
  ApiCallResponse? resultCreateEquipment;
  // Stores action output result for [Backend Call - API (CreateEquipment)] action in Button widget.
  ApiCallResponse? resultCreateEquipment0;
  // State field(s) for TabBar widget.
  TabController? tabBarController;
  int get tabBarCurrentIndex =>
      tabBarController != null ? tabBarController!.index : 0;

  // State field(s) for eqktuorequipmentdescription widget.
  TextEditingController? eqktuorequipmentdescriptionController;
  String? Function(BuildContext, String?)?
      eqktuorequipmentdescriptionControllerValidator;
  // State field(s) for object widget.
  String? objectValue;
  FormFieldController<String>? objectValueController;
  // Stores action output result for [Backend Call - API (GetClassbyEqart)] action in object widget.
  ApiCallResponse? apiResult0alCopy;
  // State field(s) for inbdtyear widget.
  String? inbdtyearValue;
  FormFieldController<String>? inbdtyearValueController;
  // State field(s) for inbdtmonth widget.
  String? inbdtmonthValue;
  FormFieldController<String>? inbdtmonthValueController;
  // State field(s) for inbdtday widget.
  String? inbdtdayValue;
  FormFieldController<String>? inbdtdayValueController;
  // State field(s) for herstormanufacturer widget.
  TextEditingController? herstormanufacturerController;
  String? Function(BuildContext, String?)?
      herstormanufacturerControllerValidator;
  // State field(s) for typbzormodelnumber widget.
  TextEditingController? typbzormodelnumberController;
  String? Function(BuildContext, String?)?
      typbzormodelnumberControllerValidator;
  // State field(s) for sergeormanufacturerserialnumber widget.
  TextEditingController? sergeormanufacturerserialnumberController;
  String? Function(BuildContext, String?)?
      sergeormanufacturerserialnumberControllerValidator;
  // State field(s) for dropdowncountrycodeofmanufacturer widget.
  String? dropdowncountrycodeofmanufacturerValue;
  FormFieldController<String>? dropdowncountrycodeofmanufacturerValueController;
  // State field(s) for baujj widget.
  String? baujjValue;
  FormFieldController<String>? baujjValueController;
  // State field(s) for baumm widget.
  String? baummValue;
  FormFieldController<String>? baummValueController;
  // State field(s) for remark widget.
  TextEditingController? remarkController;
  String? Function(BuildContext, String?)? remarkControllerValidator;
  // State field(s) for anlnrorassetno widget.
  TextEditingController? anlnrorassetnoController1;
  String? Function(BuildContext, String?)? anlnrorassetnoController1Validator;
  // State field(s) for anlnrorassetno widget.
  TextEditingController? anlnrorassetnoController2;
  String? Function(BuildContext, String?)? anlnrorassetnoController2Validator;
  // State field(s) for plannergroup widget.
  String? plannergroupValue;
  FormFieldController<String>? plannergroupValueController;
  // State field(s) for mainworkcenter widget.
  String? mainworkcenterValue;
  FormFieldController<String>? mainworkcenterValueController;
  // State field(s) for Zone widget.
  String? zoneValue;
  FormFieldController<String>? zoneValueController;
  // State field(s) for Subzone widget.
  String? subzoneValue;
  FormFieldController<String>? subzoneValueController;
  // State field(s) for SubstationDropdown widget.
  String? substationDropdownValue;
  FormFieldController<String>? substationDropdownValueController;
  // Stores action output result for [Backend Call - API (GetFLBySubstation)] action in SubstationDropdown widget.
  ApiCallResponse? subsFLE;
  // Stores action output result for [Backend Call - API (GetBay)] action in SubstationDropdown widget.
  ApiCallResponse? bayEdit;
  // State field(s) for Bay widget.
  String? bayValue;
  FormFieldController<String>? bayValueController;
  // Stores action output result for [Backend Call - API (GetFLBySubstation)] action in Bay widget.
  ApiCallResponse? bayFLE;
  // State field(s) for Primarysecondary widget.
  String? primarysecondaryValue;
  FormFieldController<String>? primarysecondaryValueController;
  // Stores action output result for [Backend Call - API (GetFLBySubstation)] action in Primarysecondary widget.
  ApiCallResponse? apiResult1acz;
  // State field(s) for objecttype widget.
  String? objecttypeValue;
  FormFieldController<String>? objecttypeValueController;
  // Stores action output result for [Backend Call - API (GetFLBySubstation)] action in objecttype widget.
  ApiCallResponse? apiResult1acz2Copy;
  // State field(s) for FLPicker widget.
  String? fLPickerValue;
  FormFieldController<String>? fLPickerValueController;
  // State field(s) for classheader widget.
  String? classheaderValue;
  FormFieldController<String>? classheaderValueController;
  // Stores action output result for [Backend Call - API (GetClassInfo)] action in classheader widget.
  ApiCallResponse? apiResult28e;
  // Models for textfield dynamic component.
  late FlutterFlowDynamicModels<TextfieldModel> textfieldModels;
  // Models for textfield2.
  late FlutterFlowDynamicModels<TextfieldModel> textfield2Models;
  bool isDataUploading1 = false;
  FFUploadedFile uploadedLocalFile1 =
      FFUploadedFile(bytes: Uint8List.fromList([]));

  bool isDataUploading2 = false;
  FFUploadedFile uploadedLocalFile2 =
      FFUploadedFile(bytes: Uint8List.fromList([]));

  /// Initialization and disposal methods.

  void initState(BuildContext context) {
    textfieldModels = FlutterFlowDynamicModels(() => TextfieldModel());
    textfield2Models = FlutterFlowDynamicModels(() => TextfieldModel());
  }

  void dispose() {
    unfocusNode.dispose();
    tabBarController?.dispose();
    eqktuorequipmentdescriptionController?.dispose();
    herstormanufacturerController?.dispose();
    typbzormodelnumberController?.dispose();
    sergeormanufacturerserialnumberController?.dispose();
    remarkController?.dispose();
    anlnrorassetnoController1?.dispose();
    anlnrorassetnoController2?.dispose();
    textfieldModels.dispose();
    textfield2Models.dispose();
  }

  /// Action blocks are added here.

  Future test(BuildContext context) async {
    addToCharcodes(classinfo.first.toString());
    addToCharvalues(textfieldModels
        .getValues(
          (m) => m.textController.text,
        )
        .first);
  }

  /// Additional helper methods are added here.
}
