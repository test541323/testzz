import '/backend/api_requests/api_calls.dart';
import '/flutter_flow/flutter_flow_drop_down.dart';
import '/flutter_flow/flutter_flow_icon_button.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import '/flutter_flow/flutter_flow_widgets.dart';
import '/flutter_flow/form_field_controller.dart';
import '/flutter_flow/custom_functions.dart' as functions;
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'equipment_search_model.dart';
export 'equipment_search_model.dart';

class EquipmentSearchWidget extends StatefulWidget {
  const EquipmentSearchWidget({Key? key}) : super(key: key);

  @override
  _EquipmentSearchWidgetState createState() => _EquipmentSearchWidgetState();
}

class _EquipmentSearchWidgetState extends State<EquipmentSearchWidget> {
  late EquipmentSearchModel _model;

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _model = createModel(context, () => EquipmentSearchModel());

    _model.textController1 ??= TextEditingController();
    _model.textController2 ??= TextEditingController();
  }

  @override
  void dispose() {
    _model.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    context.watch<FFAppState>();

    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(_model.unfocusNode),
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: FlutterFlowTheme.of(context).primaryBackground,
        appBar: AppBar(
          backgroundColor: FlutterFlowTheme.of(context).primary,
          automaticallyImplyLeading: false,
          leading: FlutterFlowIconButton(
            borderColor: Colors.transparent,
            borderRadius: 30.0,
            borderWidth: 1.0,
            buttonSize: 60.0,
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
              size: 30.0,
            ),
            onPressed: () async {
              context.safePop();
            },
          ),
          title: Text(
            'Equipment Search',
            style: FlutterFlowTheme.of(context).headlineMedium.override(
                  fontFamily: 'Outfit',
                  color: Colors.white,
                  fontSize: 22.0,
                ),
          ),
          actions: [],
          centerTitle: true,
          elevation: 2.0,
        ),
        body: SafeArea(
          top: true,
          child: Padding(
            padding: EdgeInsetsDirectional.fromSTEB(30.0, 0.0, 30.0, 0.0),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Align(
                    alignment: AlignmentDirectional(1.0, -1.0),
                    child: Padding(
                      padding:
                          EdgeInsetsDirectional.fromSTEB(0.0, 10.0, 0.0, 0.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Align(
                            alignment: AlignmentDirectional(1.0, 0.0),
                            child: FFButtonWidget(
                              onPressed: () async {
                                setState(() {
                                  _model.textController1?.clear();
                                  _model.textController2?.clear();
                                });
                                setState(() {
                                  _model.dropdownZoneValueController?.reset();
                                  _model.subzoneDropdownValueController
                                      ?.reset();
                                  _model.substationDropdownValueController
                                      ?.reset();
                                  _model.bayDropdownValueController?.reset();
                                  _model.primarySecondaryDropdownValueController
                                      ?.reset();
                                  _model.objTypeDropdownValueController
                                      ?.reset();
                                });
                              },
                              text: 'Reset',
                              options: FFButtonOptions(
                                height: 40.0,
                                padding: EdgeInsetsDirectional.fromSTEB(
                                    24.0, 0.0, 24.0, 0.0),
                                iconPadding: EdgeInsetsDirectional.fromSTEB(
                                    0.0, 0.0, 0.0, 0.0),
                                color: Color(0xFF7B7B7B),
                                textStyle: FlutterFlowTheme.of(context)
                                    .titleSmall
                                    .override(
                                      fontFamily: 'Readex Pro',
                                      color: Colors.white,
                                    ),
                                elevation: 3.0,
                                borderSide: BorderSide(
                                  color: Colors.transparent,
                                  width: 1.0,
                                ),
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                          Align(
                            alignment: AlignmentDirectional(1.0, 0.0),
                            child: FFButtonWidget(
                              onPressed: () async {
                                context.pushNamed('createequip');
                              },
                              text: 'Create',
                              options: FFButtonOptions(
                                height: 40.0,
                                padding: EdgeInsetsDirectional.fromSTEB(
                                    24.0, 0.0, 24.0, 0.0),
                                iconPadding: EdgeInsetsDirectional.fromSTEB(
                                    0.0, 0.0, 0.0, 0.0),
                                color: FlutterFlowTheme.of(context).primary,
                                textStyle: FlutterFlowTheme.of(context)
                                    .titleSmall
                                    .override(
                                      fontFamily: 'Readex Pro',
                                      color: Colors.white,
                                    ),
                                elevation: 3.0,
                                borderSide: BorderSide(
                                  color: Colors.transparent,
                                  width: 1.0,
                                ),
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                        ].divide(SizedBox(width: 5.0)),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsetsDirectional.fromSTEB(0.0, 30.0, 0.0, 0.0),
                    child: Form(
                      key: _model.formKey,
                      autovalidateMode: AutovalidateMode.disabled,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Align(
                            alignment: AlignmentDirectional(1.0, 0.0),
                            child: Padding(
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  5.0, 5.0, 15.0, 5.0),
                              child: InkWell(
                                splashColor: Colors.transparent,
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () async {
                                  setState(() {
                                    _model.dropdownZoneValueController?.reset();
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFF7B7B7B),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsetsDirectional.fromSTEB(
                                        2.0, 2.0, 2.0, 2.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Reset Zone',
                                          style: FlutterFlowTheme.of(context)
                                              .bodyMedium
                                              .override(
                                                fontFamily: 'Readex Pro',
                                                color: Colors.white,
                                                fontSize: 12.0,
                                              ),
                                        ),
                                        Icon(
                                          Icons.settings_outlined,
                                          color: Colors.white,
                                          size: 14.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          FlutterFlowDropDown<String>(
                            controller: _model.dropdownZoneValueController ??=
                                FormFieldController<String>(
                              _model.dropdownZoneValue ??= '',
                            ),
                            options: (getJsonField(
                              FFAppState().zone,
                              r'''$[:].ID''',
                            ) as List)
                                .map<String>((s) => s.toString())
                                .toList()!,
                            optionLabels: (getJsonField(
                              FFAppState().zone,
                              r'''$[:].Value''',
                            ) as List)
                                .map<String>((s) => s.toString())
                                .toList()!,
                            onChanged: (val) async {
                              setState(() => _model.dropdownZoneValue = val);
                              setState(() {
                                _model.subzoneDropdownValueController?.reset();
                                _model.substationDropdownValueController
                                    ?.reset();
                                _model.bayDropdownValueController?.reset();
                                _model.primarySecondaryDropdownValueController
                                    ?.reset();
                                _model.objTypeDropdownValueController?.reset();
                              });
                            },
                            width: 300.0,
                            height: 50.0,
                            searchHintTextStyle:
                                FlutterFlowTheme.of(context).labelMedium,
                            textStyle: FlutterFlowTheme.of(context).bodyMedium,
                            hintText: 'Zone',
                            searchHintText: 'Search for Zone...',
                            icon: Icon(
                              Icons.keyboard_arrow_down_rounded,
                              color: FlutterFlowTheme.of(context).secondaryText,
                              size: 24.0,
                            ),
                            fillColor:
                                FlutterFlowTheme.of(context).primaryBackground,
                            elevation: 1.0,
                            borderColor: Color(0xFFC5C5C5),
                            borderWidth: 1.0,
                            borderRadius: 5.0,
                            margin: EdgeInsetsDirectional.fromSTEB(
                                16.0, 4.0, 16.0, 4.0),
                            hidesUnderline: true,
                            isSearchable: true,
                            isMultiSelect: false,
                          ),
                          Align(
                            alignment: AlignmentDirectional(1.0, 0.0),
                            child: Padding(
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  5.0, 5.0, 15.0, 5.0),
                              child: InkWell(
                                splashColor: Colors.transparent,
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () async {
                                  setState(() {
                                    _model.subzoneDropdownValueController
                                        ?.reset();
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFF7B7B7B),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsetsDirectional.fromSTEB(
                                        2.0, 2.0, 2.0, 2.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Reset Subzone',
                                          style: FlutterFlowTheme.of(context)
                                              .bodyMedium
                                              .override(
                                                fontFamily: 'Readex Pro',
                                                color: Colors.white,
                                                fontSize: 12.0,
                                              ),
                                        ),
                                        Icon(
                                          Icons.settings_outlined,
                                          color: Colors.white,
                                          size: 14.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          FlutterFlowDropDown<String>(
                            controller:
                                _model.subzoneDropdownValueController ??=
                                    FormFieldController<String>(
                              _model.subzoneDropdownValue ??= '',
                            ),
                            options: functions
                                .filterjsonby(
                                    getJsonField(
                                      FFAppState().subzone,
                                      r'''$[:]''',
                                    )!,
                                    'ZoneCode',
                                    _model.dropdownZoneValue)
                                .map((e) => getJsonField(
                                      e,
                                      r'''$.INGRP''',
                                    ))
                                .toList()
                                .map((e) => e.toString())
                                .toList(),
                            optionLabels: functions.combinelist(
                                functions
                                    .filterjsonby(
                                        getJsonField(
                                          FFAppState().subzone,
                                          r'''$[:]''',
                                        )!,
                                        'ZoneCode',
                                        _model.dropdownZoneValue)
                                    .map((e) => getJsonField(
                                          e,
                                          r'''$.INGRP''',
                                        ))
                                    .toList()
                                    .map((e) => e.toString())
                                    .toList(),
                                functions
                                    .filterjsonby(
                                        getJsonField(
                                          FFAppState().subzone,
                                          r'''$[:]''',
                                        )!,
                                        'ZoneCode',
                                        _model.dropdownZoneValue)
                                    .map((e) => getJsonField(
                                          e,
                                          r'''$.INNAM''',
                                        ))
                                    .toList()
                                    .map((e) => e.toString())
                                    .toList(),
                                '  -  ')!,
                            onChanged: (val) async {
                              setState(() => _model.subzoneDropdownValue = val);
                              setState(() {
                                _model.substationDropdownValueController
                                    ?.reset();
                                _model.bayDropdownValueController?.reset();
                                _model.primarySecondaryDropdownValueController
                                    ?.reset();
                                _model.objTypeDropdownValueController?.reset();
                              });
                            },
                            width: 300.0,
                            height: 50.0,
                            searchHintTextStyle:
                                FlutterFlowTheme.of(context).labelMedium,
                            textStyle: FlutterFlowTheme.of(context).bodyMedium,
                            hintText: 'Subzone',
                            searchHintText: 'Search for an item...',
                            icon: Icon(
                              Icons.keyboard_arrow_down_rounded,
                              color: FlutterFlowTheme.of(context).secondaryText,
                              size: 24.0,
                            ),
                            fillColor:
                                FlutterFlowTheme.of(context).primaryBackground,
                            elevation: 1.0,
                            borderColor: Color(0xFFC5C5C5),
                            borderWidth: 1.0,
                            borderRadius: 5.0,
                            margin: EdgeInsetsDirectional.fromSTEB(
                                16.0, 4.0, 16.0, 4.0),
                            hidesUnderline: true,
                            isSearchable: true,
                            isMultiSelect: false,
                          ),
                          Align(
                            alignment: AlignmentDirectional(1.0, 0.0),
                            child: Padding(
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  5.0, 5.0, 15.0, 5.0),
                              child: InkWell(
                                splashColor: Colors.transparent,
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () async {
                                  setState(() {
                                    _model.substationDropdownValueController
                                        ?.reset();
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFF7B7B7B),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsetsDirectional.fromSTEB(
                                        2.0, 2.0, 2.0, 2.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Reset Substation',
                                          style: FlutterFlowTheme.of(context)
                                              .bodyMedium
                                              .override(
                                                fontFamily: 'Readex Pro',
                                                color: Colors.white,
                                                fontSize: 12.0,
                                              ),
                                        ),
                                        Icon(
                                          Icons.settings_outlined,
                                          color: Colors.white,
                                          size: 14.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          FlutterFlowDropDown<String>(
                            controller:
                                _model.substationDropdownValueController ??=
                                    FormFieldController<String>(
                              _model.substationDropdownValue ??= '',
                            ),
                            options: functions
                                .filterjsonby(
                                    getJsonField(
                                      FFAppState().substation,
                                      r'''$[:]''',
                                    )!,
                                    'INGRP',
                                    _model.subzoneDropdownValue)
                                .map((e) => getJsonField(
                                      e,
                                      r'''$.FL''',
                                    ))
                                .toList()
                                .map((e) => e.toString())
                                .toList(),
                            optionLabels: functions.combinelist(
                                functions
                                    .filterjsonby(
                                        getJsonField(
                                          FFAppState().substation,
                                          r'''$[:]''',
                                        )!,
                                        'INGRP',
                                        _model.subzoneDropdownValue)
                                    .map((e) => getJsonField(
                                          e,
                                          r'''$.FL''',
                                        ))
                                    .toList()
                                    .map((e) => e.toString())
                                    .toList(),
                                functions
                                    .filterjsonby(
                                        getJsonField(
                                          FFAppState().substation,
                                          r'''$[:]''',
                                        )!,
                                        'INGRP',
                                        _model.subzoneDropdownValue)
                                    .map((e) => getJsonField(
                                          e,
                                          r'''$.SubstationDesc''',
                                        ))
                                    .toList()
                                    .map((e) => e.toString())
                                    .toList(),
                                '  -  ')!,
                            onChanged: (val) async {
                              setState(
                                  () => _model.substationDropdownValue = val);
                              setState(() {
                                _model.bayDropdownValueController?.reset();
                                _model.primarySecondaryDropdownValueController
                                    ?.reset();
                                _model.objTypeDropdownValueController?.reset();
                              });
                              _model.apiResulta7n = await GetBayCall.call(
                                urlendpoint: FFAppState().urlendpoint,
                                substationFL: _model.substationDropdownValue,
                              );
                              if ((_model.apiResulta7n?.succeeded ?? true)) {
                                setState(() {
                                  _model.bay = getJsonField(
                                    (_model.apiResulta7n?.jsonBody ?? ''),
                                    r'''$[:]''',
                                  )!
                                      .toList()
                                      .cast<dynamic>();
                                });
                              }

                              setState(() {});
                            },
                            width: 300.0,
                            height: 50.0,
                            searchHintTextStyle:
                                FlutterFlowTheme.of(context).labelMedium,
                            textStyle: FlutterFlowTheme.of(context).bodyMedium,
                            hintText: 'Substation',
                            searchHintText: 'Search for an item...',
                            icon: Icon(
                              Icons.keyboard_arrow_down_rounded,
                              color: FlutterFlowTheme.of(context).secondaryText,
                              size: 24.0,
                            ),
                            fillColor:
                                FlutterFlowTheme.of(context).primaryBackground,
                            elevation: 1.0,
                            borderColor: Color(0xFFC5C5C5),
                            borderWidth: 1.0,
                            borderRadius: 5.0,
                            margin: EdgeInsetsDirectional.fromSTEB(
                                16.0, 4.0, 16.0, 4.0),
                            hidesUnderline: true,
                            isSearchable: true,
                            isMultiSelect: false,
                          ),
                          Align(
                            alignment: AlignmentDirectional(1.0, 0.0),
                            child: Padding(
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  5.0, 5.0, 15.0, 5.0),
                              child: InkWell(
                                splashColor: Colors.transparent,
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () async {
                                  setState(() {
                                    _model.bayDropdownValueController?.reset();
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFF7B7B7B),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsetsDirectional.fromSTEB(
                                        2.0, 2.0, 2.0, 2.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Reset Bay',
                                          style: FlutterFlowTheme.of(context)
                                              .bodyMedium
                                              .override(
                                                fontFamily: 'Readex Pro',
                                                color: Colors.white,
                                                fontSize: 12.0,
                                              ),
                                        ),
                                        Icon(
                                          Icons.settings_outlined,
                                          color: Colors.white,
                                          size: 14.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          FlutterFlowDropDown<String>(
                            controller: _model.bayDropdownValueController ??=
                                FormFieldController<String>(
                              _model.bayDropdownValue ??= '',
                            ),
                            options: functions
                                .filterduplicatevaluejson(
                                    _model.bay.toList(), 'TPLNR')!
                                .map((e) => getJsonField(
                                      e,
                                      r'''$.TPLNR''',
                                    ))
                                .toList()
                                .map((e) => e.toString())
                                .toList(),
                            optionLabels: functions.combinelist(
                                functions
                                    .filterjsonby(
                                        functions
                                            .filterduplicatevaluejson(
                                                _model.bay.toList(), 'TPLNR')!
                                            .toList(),
                                        'TPLNR',
                                        _model.substationDropdownValue)
                                    .map((e) => getJsonField(
                                          e,
                                          r'''$.TPLNR''',
                                        ))
                                    .toList()
                                    .map((e) => e.toString())
                                    .toList(),
                                functions
                                    .filterjsonby(
                                        functions
                                            .filterduplicatevaluejson(
                                                _model.bay.toList(), 'TPLNR')!
                                            .toList(),
                                        'TPLNR',
                                        _model.substationDropdownValue)
                                    .map((e) => getJsonField(
                                          e,
                                          r'''$.BayDesc''',
                                        ))
                                    .toList()
                                    .map((e) => e.toString())
                                    .toList(),
                                '  -  ')!,
                            onChanged: (val) async {
                              setState(() => _model.bayDropdownValue = val);
                              setState(() {
                                _model.primarySecondaryDropdownValueController
                                    ?.reset();
                                _model.objTypeDropdownValueController?.reset();
                              });
                            },
                            width: 300.0,
                            height: 50.0,
                            searchHintTextStyle:
                                FlutterFlowTheme.of(context).labelMedium,
                            textStyle: FlutterFlowTheme.of(context).bodyMedium,
                            hintText: 'Bay',
                            searchHintText: 'Search for an item...',
                            icon: Icon(
                              Icons.keyboard_arrow_down_rounded,
                              color: FlutterFlowTheme.of(context).secondaryText,
                              size: 24.0,
                            ),
                            fillColor:
                                FlutterFlowTheme.of(context).primaryBackground,
                            elevation: 1.0,
                            borderColor: Color(0xFFC5C5C5),
                            borderWidth: 1.0,
                            borderRadius: 5.0,
                            margin: EdgeInsetsDirectional.fromSTEB(
                                16.0, 4.0, 16.0, 4.0),
                            hidesUnderline: true,
                            isSearchable: true,
                            isMultiSelect: false,
                          ),
                          Align(
                            alignment: AlignmentDirectional(1.0, 0.0),
                            child: Padding(
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  5.0, 5.0, 15.0, 5.0),
                              child: InkWell(
                                splashColor: Colors.transparent,
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () async {
                                  setState(() {
                                    _model
                                        .primarySecondaryDropdownValueController
                                        ?.reset();
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFF7B7B7B),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsetsDirectional.fromSTEB(
                                        2.0, 2.0, 2.0, 2.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Reset Primary/Secondary',
                                          style: FlutterFlowTheme.of(context)
                                              .bodyMedium
                                              .override(
                                                fontFamily: 'Readex Pro',
                                                color: Colors.white,
                                                fontSize: 12.0,
                                              ),
                                        ),
                                        Icon(
                                          Icons.settings_outlined,
                                          color: Colors.white,
                                          size: 14.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          FlutterFlowDropDown<String>(
                            controller: _model
                                    .primarySecondaryDropdownValueController ??=
                                FormFieldController<String>(
                              _model.primarySecondaryDropdownValue ??= '%',
                            ),
                            options: ['_A', '_B', '%'],
                            optionLabels: [
                              'A  -  Primary',
                              'B  -  Secondary',
                              'ALL'
                            ],
                            onChanged: (val) async {
                              setState(() =>
                                  _model.primarySecondaryDropdownValue = val);
                              setState(() {
                                _model.objTypeDropdownValueController?.reset();
                              });
                            },
                            width: 300.0,
                            height: 50.0,
                            searchHintTextStyle:
                                FlutterFlowTheme.of(context).labelMedium,
                            textStyle: FlutterFlowTheme.of(context).bodyMedium,
                            hintText: 'Primary / Secondary',
                            searchHintText: 'Search for an item...',
                            icon: Icon(
                              Icons.keyboard_arrow_down_rounded,
                              color: FlutterFlowTheme.of(context).secondaryText,
                              size: 24.0,
                            ),
                            fillColor:
                                FlutterFlowTheme.of(context).primaryBackground,
                            elevation: 1.0,
                            borderColor: Color(0xFFC5C5C5),
                            borderWidth: 1.0,
                            borderRadius: 5.0,
                            margin: EdgeInsetsDirectional.fromSTEB(
                                16.0, 4.0, 16.0, 4.0),
                            hidesUnderline: true,
                            isSearchable: true,
                            isMultiSelect: false,
                          ),
                          Align(
                            alignment: AlignmentDirectional(1.0, 0.0),
                            child: Padding(
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  5.0, 5.0, 15.0, 5.0),
                              child: InkWell(
                                splashColor: Colors.transparent,
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () async {
                                  setState(() {
                                    _model.objTypeDropdownValueController
                                        ?.reset();
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFF7B7B7B),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsetsDirectional.fromSTEB(
                                        2.0, 2.0, 2.0, 2.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Reset Object Type',
                                          style: FlutterFlowTheme.of(context)
                                              .bodyMedium
                                              .override(
                                                fontFamily: 'Readex Pro',
                                                color: Colors.white,
                                                fontSize: 12.0,
                                              ),
                                        ),
                                        Icon(
                                          Icons.settings_outlined,
                                          color: Colors.white,
                                          size: 14.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          FlutterFlowDropDown<String>(
                            controller:
                                _model.objTypeDropdownValueController ??=
                                    FormFieldController<String>(
                              _model.objTypeDropdownValue ??= '',
                            ),
                            options: functions
                                .filterduplicatevaluejson(
                                    getJsonField(
                                      FFAppState().objecttype,
                                      r'''$[:]''',
                                    ),
                                    'ObjectTypePrefix')!
                                .map((e) => getJsonField(
                                      e,
                                      r'''$.ObjectTypePrefix''',
                                    ))
                                .toList()
                                .map((e) => e.toString())
                                .toList(),
                            optionLabels: functions.combinelist(
                                functions
                                    .filterduplicatevaluejson(
                                        getJsonField(
                                          FFAppState().objecttype,
                                          r'''$[:]''',
                                        ),
                                        'ObjectTypePrefix')
                                    ?.map((e) => getJsonField(
                                          e,
                                          r'''$.ObjectTypePrefix''',
                                        ))
                                    .toList()
                                    ?.map((e) => e.toString())
                                    .toList()
                                    ?.toList(),
                                functions
                                    .filterduplicatevaluejson(
                                        getJsonField(
                                          FFAppState().objecttype,
                                          r'''$[:]''',
                                        ),
                                        'ObjectTypePrefix')
                                    ?.map((e) => getJsonField(
                                          e,
                                          r'''$.ObjectTypeText''',
                                        ))
                                    .toList()
                                    ?.map((e) => e.toString())
                                    .toList()
                                    ?.toList(),
                                '  -  ')!,
                            onChanged: (val) => setState(
                                () => _model.objTypeDropdownValue = val),
                            width: 300.0,
                            height: 50.0,
                            searchHintTextStyle:
                                FlutterFlowTheme.of(context).labelMedium,
                            textStyle: FlutterFlowTheme.of(context).bodyMedium,
                            hintText: 'Object Type',
                            searchHintText: 'Search for an item...',
                            icon: Icon(
                              Icons.keyboard_arrow_down_rounded,
                              color: FlutterFlowTheme.of(context).secondaryText,
                              size: 24.0,
                            ),
                            fillColor:
                                FlutterFlowTheme.of(context).primaryBackground,
                            elevation: 1.0,
                            borderColor: Color(0xFFC5C5C5),
                            borderWidth: 1.0,
                            borderRadius: 5.0,
                            margin: EdgeInsetsDirectional.fromSTEB(
                                16.0, 4.0, 16.0, 4.0),
                            hidesUnderline: true,
                            isSearchable: true,
                            isMultiSelect: false,
                          ),
                          if (valueOrDefault<String>(
                                    functions.flfromdropdown(
                                        _model.substationDropdownValue,
                                        _model.bayDropdownValue,
                                        _model.primarySecondaryDropdownValue,
                                        _model.objTypeDropdownValue),
                                    'Click substation to start search by substation',
                                  ) !=
                                  null &&
                              valueOrDefault<String>(
                                    functions.flfromdropdown(
                                        _model.substationDropdownValue,
                                        _model.bayDropdownValue,
                                        _model.primarySecondaryDropdownValue,
                                        _model.objTypeDropdownValue),
                                    'Click substation to start search by substation',
                                  ) !=
                                  '')
                            Padding(
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  0.0, 10.0, 0.0, 0.0),
                              child: Text(
                                valueOrDefault<String>(
                                  functions.flfromdropdown(
                                      _model.substationDropdownValue,
                                      _model.bayDropdownValue,
                                      _model.primarySecondaryDropdownValue,
                                      _model.objTypeDropdownValue),
                                  'Click substation to start search by substation',
                                ),
                                style: FlutterFlowTheme.of(context)
                                    .bodyMedium
                                    .override(
                                      fontFamily: 'Readex Pro',
                                      fontSize: 20.0,
                                    ),
                              ),
                            ),
                          Padding(
                            padding: EdgeInsetsDirectional.fromSTEB(
                                30.0, 20.0, 30.0, 0.0),
                            child: TextFormField(
                              controller: _model.textController1,
                              autofocus: true,
                              obscureText: false,
                              decoration: InputDecoration(
                                labelText: 'Serial No',
                                labelStyle:
                                    FlutterFlowTheme.of(context).labelMedium,
                                hintStyle:
                                    FlutterFlowTheme.of(context).labelMedium,
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: FlutterFlowTheme.of(context)
                                        .primaryText,
                                    width: 2.0,
                                  ),
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: FlutterFlowTheme.of(context).primary,
                                    width: 2.0,
                                  ),
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                errorBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: FlutterFlowTheme.of(context).error,
                                    width: 2.0,
                                  ),
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                focusedErrorBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: FlutterFlowTheme.of(context).error,
                                    width: 2.0,
                                  ),
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                contentPadding: EdgeInsetsDirectional.fromSTEB(
                                    30.0, 0.0, 0.0, 0.0),
                              ),
                              style: FlutterFlowTheme.of(context).bodyMedium,
                              validator: _model.textController1Validator
                                  .asValidator(context),
                            ),
                          ),
                          if (false)
                            Padding(
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  30.0, 20.0, 30.0, 0.0),
                              child: TextFormField(
                                controller: _model.textController2,
                                autofocus: true,
                                obscureText: false,
                                decoration: InputDecoration(
                                  labelText: 'Equipment Number',
                                  labelStyle:
                                      FlutterFlowTheme.of(context).labelMedium,
                                  hintStyle:
                                      FlutterFlowTheme.of(context).labelMedium,
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: FlutterFlowTheme.of(context)
                                          .primaryText,
                                      width: 2.0,
                                    ),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color:
                                          FlutterFlowTheme.of(context).primary,
                                      width: 2.0,
                                    ),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  errorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: FlutterFlowTheme.of(context).error,
                                      width: 2.0,
                                    ),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  focusedErrorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: FlutterFlowTheme.of(context).error,
                                      width: 2.0,
                                    ),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  contentPadding:
                                      EdgeInsetsDirectional.fromSTEB(
                                          30.0, 0.0, 0.0, 0.0),
                                ),
                                style: FlutterFlowTheme.of(context).bodyMedium,
                                validator: _model.textController2Validator
                                    .asValidator(context),
                              ),
                            ),
                          Padding(
                            padding: EdgeInsetsDirectional.fromSTEB(
                                0.0, 40.0, 0.0, 0.0),
                            child: FFButtonWidget(
                              onPressed: () async {
                                if (_model.textController2.text != null &&
                                    _model.textController2.text != '') {
                                  context.pushNamed(
                                    'equipment',
                                    queryParameters: {
                                      'equnr': serializeParam(
                                        _model.textController2.text,
                                        ParamType.String,
                                      ),
                                    }.withoutNulls,
                                  );
                                } else {
                                  if (_model.textController1.text != null &&
                                      _model.textController1.text != '') {
                                    _model.getequipmentbyserialno1 =
                                        await GetEquipmentBySerialNoCall.call(
                                      serialNo: _model.textController1.text,
                                      urlendpoint: FFAppState().urlendpoint,
                                    );
                                    setState(() {
                                      FFAppState().listequipmentbyserialno =
                                          getJsonField(
                                        (_model.getequipmentbyserialno1
                                                ?.jsonBody ??
                                            ''),
                                        r'''$[:]''',
                                      )!
                                              .toList()
                                              .cast<dynamic>();
                                    });

                                    context.pushNamed(
                                      'searchbyserialno',
                                      queryParameters: {
                                        'uid': serializeParam(
                                          getJsonField(
                                            FFAppState().alluserinfo,
                                            r'''$.EmployeeID''',
                                          ).toString(),
                                          ParamType.String,
                                        ),
                                      }.withoutNulls,
                                    );
                                  } else {
                                    if (true) {
                                      _model.apiResultListing =
                                          await GetEquipmentBySubstationCall
                                              .call(
                                        substationFL: valueOrDefault<String>(
                                          functions.flfromdropdown(
                                              _model.substationDropdownValue,
                                              _model.bayDropdownValue,
                                              _model
                                                  .primarySecondaryDropdownValue,
                                              _model.objTypeDropdownValue),
                                          'Click substation to start search by substation',
                                        ),
                                        urlendpoint: FFAppState().urlendpoint,
                                      );
                                      if ((_model.apiResultListing?.succeeded ??
                                          true)) {
                                        FFAppState().listequipmentbyserialno =
                                            GetEquipmentBySubstationCall.all(
                                          (_model.apiResultListing?.jsonBody ??
                                              ''),
                                        )!
                                                .toList()
                                                .cast<dynamic>();

                                        context.pushNamed(
                                          'searchbyserialno',
                                          queryParameters: {
                                            'uid': serializeParam(
                                              getJsonField(
                                                FFAppState().alluserinfo,
                                                r'''$.EmployeeID''',
                                              ).toString(),
                                              ParamType.String,
                                            ),
                                          }.withoutNulls,
                                        );
                                      }
                                    }
                                  }
                                }

                                setState(() {});
                              },
                              text: 'Search',
                              options: FFButtonOptions(
                                height: 40.0,
                                padding: EdgeInsetsDirectional.fromSTEB(
                                    24.0, 0.0, 24.0, 0.0),
                                iconPadding: EdgeInsetsDirectional.fromSTEB(
                                    0.0, 0.0, 0.0, 0.0),
                                color: FlutterFlowTheme.of(context).primary,
                                textStyle: FlutterFlowTheme.of(context)
                                    .titleSmall
                                    .override(
                                      fontFamily: 'Readex Pro',
                                      color: Colors.white,
                                    ),
                                elevation: 3.0,
                                borderSide: BorderSide(
                                  color: Colors.transparent,
                                  width: 1.0,
                                ),
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
