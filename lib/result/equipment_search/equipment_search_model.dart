import '/backend/api_requests/api_calls.dart';
import '/flutter_flow/flutter_flow_drop_down.dart';
import '/flutter_flow/flutter_flow_icon_button.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import '/flutter_flow/flutter_flow_widgets.dart';
import '/flutter_flow/form_field_controller.dart';
import '/flutter_flow/custom_functions.dart' as functions;
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class EquipmentSearchModel extends FlutterFlowModel {
  ///  Local state fields for this page.

  List<dynamic> bay = [];
  void addToBay(dynamic item) => bay.add(item);
  void removeFromBay(dynamic item) => bay.remove(item);
  void removeAtIndexFromBay(int index) => bay.removeAt(index);
  void updateBayAtIndex(int index, Function(dynamic) updateFn) =>
      bay[index] = updateFn(bay[index]);

  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();
  final formKey = GlobalKey<FormState>();
  // State field(s) for DropdownZone widget.
  String? dropdownZoneValue;
  FormFieldController<String>? dropdownZoneValueController;
  // State field(s) for SubzoneDropdown widget.
  String? subzoneDropdownValue;
  FormFieldController<String>? subzoneDropdownValueController;
  // State field(s) for SubstationDropdown widget.
  String? substationDropdownValue;
  FormFieldController<String>? substationDropdownValueController;
  // Stores action output result for [Backend Call - API (GetBay)] action in SubstationDropdown widget.
  ApiCallResponse? apiResulta7n;
  // State field(s) for BayDropdown widget.
  String? bayDropdownValue;
  FormFieldController<String>? bayDropdownValueController;
  // State field(s) for PrimarySecondaryDropdown widget.
  String? primarySecondaryDropdownValue;
  FormFieldController<String>? primarySecondaryDropdownValueController;
  // State field(s) for ObjTypeDropdown widget.
  String? objTypeDropdownValue;
  FormFieldController<String>? objTypeDropdownValueController;
  // State field(s) for TextField widget.
  TextEditingController? textController1;
  String? Function(BuildContext, String?)? textController1Validator;
  // State field(s) for TextField widget.
  TextEditingController? textController2;
  String? Function(BuildContext, String?)? textController2Validator;
  // Stores action output result for [Backend Call - API (GetEquipmentBySerialNo)] action in Button widget.
  ApiCallResponse? getequipmentbyserialno1;
  // Stores action output result for [Backend Call - API (GetEquipmentBySubstation)] action in Button widget.
  ApiCallResponse? apiResultListing;

  /// Initialization and disposal methods.

  void initState(BuildContext context) {}

  void dispose() {
    unfocusNode.dispose();
    textController1?.dispose();
    textController2?.dispose();
  }

  /// Action blocks are added here.

  /// Additional helper methods are added here.
}
