import 'dart:convert';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'lat_lng.dart';
import 'place.dart';
import 'uploaded_file.dart';

String? dateformat(String? originalDate) {
  if (originalDate != null) {
    DateTime dateTime = DateTime.parse(originalDate);
    String formattedDate = DateFormat('M/d/yyyy h:mm:ss a').format(dateTime);
    return formattedDate;
  }
}

List<dynamic> emptylistjson() {
  return [];
}

List<bool>? emptylistbool() {
  return [true];
}

bool? isitsamelisttolist(
  List<String>? stringA,
  List<String>? stringB,
) {
  if (stringA != null && stringB != null) {
    final trimmedA = stringA.map((item) => item.trim()).toList();
    final trimmedB = stringB.map((item) => item.trim()).toList();

    for (String itemA in trimmedA) {
      if (trimmedB.contains(itemA)) {
        return true;
      }
    }
    return false;
  }
  return null;
}

bool? isitsamelist(
  String? stringA,
  List<String>? stringB,
) {
  if (stringA != null && stringB != null) {
    final trimmedA = stringA.trim();
    for (String item in stringB) {
      final trimmedItem = item.trim();
      if (trimmedA == trimmedItem) {
        return true;
      }
    }
    return false;
  }
  return null;
}

List<dynamic>? distinctlist(dynamic test) {
  List<String> inputList = List<String>.from(test);
  List<String> distinctList = inputList.toSet().toList();
  return distinctList;
}

String? colortohex(String colorName) {
  String hexColor = '';
  switch (colorName.toLowerCase()) {
    case 'red':
      hexColor = '#FF0000';
      break;
    case 'green':
      hexColor = '#00FF00';
      break;
    case 'blue':
      hexColor = '#0000FF';
      break;
    case 'yellow':
      hexColor = '#FFFF00';
      break;
    case 'orange':
      hexColor = '#FFA500';
      break;
    case 'purple':
      hexColor = '#800080';
      break;
    case 'pink':
      hexColor = '#FFC0CB';
      break;
    case 'brown':
      hexColor = '#A52A2A';
      break;
    case 'black':
      hexColor = '#000000';
      break;
    case 'white':
      hexColor = '#FFFFFF';
      break;
    default:
      print('Color not found!');
  }
  return hexColor;
}

dynamic tokentojson(String token) {
  try {
    final parts = token.split('.');
    final payload = parts[1];

    final normalizedPayload = base64Url.normalize(payload);
    final decodedPayload = utf8.decode(base64Url.decode(normalizedPayload));

    final payloadData = json.decode(decodedPayload);

    final username = payloadData['sub'].toString().replaceAll('\\\\', '\\');
    final expiration = payloadData['exp'] as int?;

    if (username != null && expiration != null) {
      final expirationDate =
          DateTime.fromMillisecondsSinceEpoch(expiration * 1000);
      final formattedExpiration =
          DateFormat.yMMMMd().add_jms().format(expirationDate);
      final timeAgo = timeago.format(expirationDate);

      final result = [
        {
          'username': username,
          'expiration': formattedExpiration,
        }
      ];
      final jsonString = json.encode(result);
      return jsonString;
    }
  } catch (e) {
    print('Error parsing JWT token: $e');
  }
  return null;
}

String? uriencode(String input) {
  try {
    final encodedInput = Uri.encodeComponent(input);
    return encodedInput;
  } catch (e) {
    print('Error encoding input: $e');
  }
  return null;
}

String? tokentousername(String token) {
  try {
    final parts = token.split('.');
    final payload = parts[1];

    final normalizedPayload = base64Url.normalize(payload);
    final decodedPayload = utf8.decode(base64Url.decode(normalizedPayload));

    final payloadData = json.decode(decodedPayload);

    final username = payloadData['sub'].toString().replaceAll('\\\\', '\\');
    return username;
  } catch (e) {
    print('Error parsing JWT token: $e');
  }

  return null;
}

DateTime? tokentoexpieration(String token) {
  try {
    final parts = token.split('.');
    final payload = parts[1];

    final normalizedPayload = base64Url.normalize(payload);
    final decodedPayload = utf8.decode(base64Url.decode(normalizedPayload));

    final payloadData = json.decode(decodedPayload);

    final expiration = payloadData['exp'] as int?;

    if (expiration != null) {
      final expirationDate =
          DateTime.fromMillisecondsSinceEpoch(expiration * 1000);

      return expirationDate;
    }
  } catch (e) {
    print('Error parsing JWT token: $e');
  }
  return null;
}

String doubletosingleslash(String doubleslash) {
  return doubleslash.replaceAll('\\\\', '\\');
}

List<String>? combinelist(
  List<String>? code,
  List<String>? desc,
  String? separator,
) {
  List<String>? output = [];

  if (code == null || desc == null || separator == null) {
    return null;
  }

  int minLength = math.min(code.length, desc.length);

  for (int i = 0; i < minLength; i++) {
    output.add("${code[i]} $separator ${desc[i]}");
  }

  return output;
}

List<dynamic> generatemonth() {
  List<Map<String, String>> monthList = [];

  for (int month = 12; month >= 1; month--) {
    DateTime date = DateTime(2023, month);
    String monthValue = DateFormat('MM').format(date);
    String monthName = DateFormat('MMMM').format(date);

    monthList.add({'value': monthValue, 'name': monthName});
  }

  return monthList;
}

String? encodeparameter2(
  String uid,
  String? fl,
  String? fldesc,
  String? eqdesc,
  String? manufacturer,
  String? modelno,
  String? serialno,
  String? objcode,
  String? objdesc,
  String? startupdate,
  String? constructyear,
  String? constructmonth,
  String? countrycode,
  String? countryname,
  String? businessarea,
  String? assetno,
  String? assetsubno,
  String? costcenter,
  String? plannergroupcode,
  String? plannergroupdesc,
  String? mainworkcentercode,
  String? mainworkcenterdesc,
  String? eqno,
  List<String>? classheadercode,
  List<String>? charcodes,
  List<String>? charvalues,
  List<String>? classheadercode2,
  List<String>? charcodes2,
  List<String>? charvalues2,
  String? remark,
) {
// Function to remove spaces from each element in a list
  List<String> removeSpacesFromList(List<String>? inputList) {
    return inputList?.map((item) => item.replaceAll(' ', '')).toList() ?? [];
  }

  // Combine lists from classheadercode and classheadercode2
  final combinedClassheadercode = <String>[];
  if (classheadercode != null) {
    combinedClassheadercode.addAll(removeSpacesFromList(classheadercode));
  }
  if (classheadercode2 != null) {
    combinedClassheadercode.addAll(removeSpacesFromList(classheadercode2));
  }

  // Combine lists from charcodes and charcodes2
  final combinedCharcodes = <String>[];
  if (charcodes != null) {
    combinedCharcodes.addAll(charcodes);
  }
  if (charcodes2 != null) {
    combinedCharcodes.addAll(charcodes2);
  }

  // Combine lists from charvalues and charvalues2
  final combinedCharvalues = <String>[];
  if (charvalues != null) {
    combinedCharvalues.addAll(charvalues);
  }
  if (charvalues2 != null) {
    combinedCharvalues.addAll(charvalues2);
  }

  final Map<String, dynamic> parameters = {
    'uid': uid,
    'fl': fl,
    'fldesc': fldesc,
    'eqdesc': eqdesc,
    'manufacturer': manufacturer,
    'modelno': modelno,
    'serialno': serialno,
    'objcode': objcode,
    'objdesc': objdesc,
    'startupdate': startupdate,
    'constructyear': constructyear,
    'constructmonth': constructmonth,
    'countrycode': countrycode,
    'countryname': countryname,
    'businessarea': businessarea,
    'assetno': assetno,
    'assetsubno': assetsubno,
    'costcenter': costcenter,
    'plannergroupcode': plannergroupcode,
    'plannergroupdesc': plannergroupdesc,
    'mainworkcentercode': mainworkcentercode,
    'mainworkcenterdesc': mainworkcenterdesc,
    'eqno': eqno,
    'classheadercodes': combinedClassheadercode,
    'charcodes': combinedCharcodes,
    'charvalues': combinedCharvalues,
    'remark': remark,
  };
  final encodedParameters = Uri(queryParameters: parameters).query;
  return encodedParameters;
}

List<String> generateday(
  String year,
  String month,
) {
  int selectedYear = int.parse(year);
  int selectedMonth = int.parse(month);

  DateTime startDate = DateTime(selectedYear, selectedMonth, 1);
  DateTime endDate = DateTime(selectedYear, selectedMonth + 1, 0);

  List<String> dayList = [];

  for (int day = 1; day <= endDate.day; day++) {
    DateTime date = DateTime(selectedYear, selectedMonth, day);
    String formattedDay = DateFormat('dd').format(date);

    dayList.add(formattedDay);
  }

  return dayList;
}

List<String> generatefromyear(int fromYear) {
  DateTime now = DateTime.now();
  int currentYear = now.year;
  List<String> yearList = [];

  for (int year = currentYear; year >= fromYear; year--) {
    yearList.add(year.toString());
  }

  return yearList;
}

String inbdttoday(String inbdt) {
  String inbdtString = inbdt.toString();
  String dayString = inbdtString.substring(6, 8);
  String formattedDay = dayString.padLeft(2, '0');
  return formattedDay;
}

String inbdttomonth(String inbdt) {
  String inbdtString = inbdt.toString();
  String monthString = inbdtString.substring(4, 6);
  String formattedMonth = monthString.padLeft(2, '0');
  return formattedMonth;
}

List<String>? prefixlist(List<dynamic>? objecttype) {
  return objecttype
      ?.map((obj) =>
          obj != null ? (obj["ObjectTypePrefix"]?.toString() ?? '') : '')
      .toList();
}

List<bool>? checklistsamestring(
  List<String>? list,
  String? string,
) {
  List<bool>? output = [];

  if (list == null || string == null) {
    return null;
  }

  for (int i = 0; i < list.length; i++) {
    output.add(list[i] == string);
  }

  return output;
}

int? findindex(
  List<String>? listitem,
  String? value,
) {
  return null;
}

bool checkjsoncontainword(
  dynamic jsondata,
  String? value,
) {
  String jsonDataString = json
      .encode(jsondata)
      .toLowerCase(); // Convert the jsondata to lowercase String
  value = value?.toLowerCase(); // Convert the value to lowercase
  return jsonDataString.contains(value ??
      ''); // Check if the lowercase String contains the lowercase value
}

String? prefix(dynamic objecttype) {
  return objecttype["ObjectTypePrefix"] ?? '';
}

int inbdttoyear(String inbdt) {
  String inbdtString = inbdt.toString();
  String yearString = inbdtString.substring(0, 4);
  int year = int.parse(yearString);
  return year;
}

List<dynamic> jsonfromcharid(
  List<dynamic> listA,
  String listB,
) {
  List<dynamic> filteredList =
      listA.where((item) => item['CharID'].trim() == listB.trim()).toList();
  return filteredList;
}

List<int>? generateyear(
  int fromYear,
  int toYear,
) {
  List<int> yearList = [];
  for (int year = fromYear; year <= toYear; year++) {
    yearList.add(year);
  }

  return yearList;
}

List<dynamic> filterlistobjecttypebylisteqart(
  List<dynamic> listA,
  List<String> listB,
) {
  List<String> trimmedListB = listB.map((item) => item.trim()).toList();
  List<dynamic> filteredList = listA.where((item) {
    return trimmedListB.contains(item['CLASS'].trim());
  }).toList();
  return filteredList;
}

List<dynamic> jsonfromcode(
  List<dynamic> listA,
  String listB,
) {
  List<dynamic> filteredList =
      listA.where((item) => item['Code'].trim() == listB.trim()).toList();
  return filteredList;
}

String guid() {
  final random = math.Random();
  final charSet =
      '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  final segments = [8, 4, 4, 4, 12];
  final buffer = StringBuffer();

  for (final segmentLength in segments) {
    for (var i = 0; i < segmentLength; i++) {
      final randomIndex = random.nextInt(charSet.length);
      buffer.write(charSet[randomIndex]);
    }
    buffer.write('-');
  }

  final guid = buffer.toString().substring(0, buffer.length - 1);
  return guid;
}

String addprefixtnb(
  String userid,
  String prefix,
) {
  if (userid != null && !userid.startsWith(prefix ?? '')) {
    return '$prefix$userid';
  }
  return userid;
}

String removespace(String somevalue) {
  String withoutSpaces = somevalue.replaceAll(' ', '');
  return withoutSpaces;
}

String? headercombinefromlist(
  List<String>? charcodes,
  List<String>? charvalues,
) {
  if (charcodes == null || charvalues == null) {
    return null;
  }
  if (charcodes.length != charvalues.length) {
    throw Exception('charcodes and charvalues must have the same length');
  }

  final items = <String>[];
  for (var i = 0; i < charcodes.length; i++) {
    final charcode = charcodes[i];
    final charvalue = charvalues[i];

    if (charcode.isNotEmpty && charvalue.isNotEmpty) {
      items.add('charcodes=${Uri.encodeQueryComponent(charcode)}');
      items.add('charvalues=${Uri.encodeQueryComponent(charvalue)}');
    }
  }

  if (items.isEmpty) {
    return null;
  }

  return '&${items.join('&')}';
}

String encodeparameter(
  String uid,
  String? fl,
  String? fldesc,
  String? eqdesc,
  String? manufacturer,
  String? modelno,
  String? serialno,
  String? objcode,
  String? objdesc,
  String? startupdate,
  String? constructyear,
  String? constructmonth,
  String? countrycode,
  String? countryname,
  String? businessarea,
  String? assetno,
  String? assetsubno,
  String? costcenter,
  String? plannergroupcode,
  String? plannergroupdesc,
  String? mainworkcentercode,
  String? mainworkcenterdesc,
  String? eqno,
  String? classheadercode,
  List<String>? charcodes,
  List<String>? charvalues,
  String? remark,
) {
  final Map<String, dynamic> parameters = {
    'uid': uid,
    'fl': fl,
    'fldesc': fldesc,
    'eqdesc': eqdesc,
    'manufacturer': manufacturer,
    'modelno': modelno,
    'serialno': serialno,
    'objcode': objcode,
    'objdesc': objdesc,
    'startupdate': startupdate,
    'constructyear': constructyear,
    'constructmonth': constructmonth,
    'countrycode': countrycode,
    'countryname': countryname,
    'businessarea': businessarea,
    'assetno': assetno,
    'assetsubno': assetsubno,
    'costcenter': costcenter,
    'plannergroupcode': plannergroupcode,
    'plannergroupdesc': plannergroupdesc,
    'mainworkcentercode': mainworkcentercode,
    'mainworkcenterdesc': mainworkcenterdesc,
    'eqno': eqno,
    'classheadercode': classheadercode,
    'charcodes': charcodes,
    'charvalues': charvalues,
    'remark': remark,
  };
  final encodedParameters = Uri(queryParameters: parameters).query;
  return encodedParameters;
}

List<String>? optionvalue(List<String>? optioninput) {
  List<String>? outputList = [];

  if (optioninput != null) {
    for (String? input in optioninput) {
      if (input?.trim().toLowerCase().contains('e') == true) {
        double? value = double.tryParse(input!);
        if (value != null) {
          outputList.add(value.toInt().toString());
        } else {
          outputList.add(input!);
        }
      } else {
        outputList.add(input!);
      }
    }
  }

  return outputList;
}

List<String>? fromlisttolist(
  List<String>? listA,
  List<String>? listB,
) {
  List<String>? outputList = [];

  if (listA != null && listB != null) {
    for (int i = 0; i < listA.length; i++) {
      String? elementA = listA[i];
      String? elementB = listB[i];

      if (elementA != null && elementA.isNotEmpty) {
        outputList.add(elementA);
      } else if (elementB != null && elementB.isNotEmpty) {
        outputList.add(elementB);
      } else {
        outputList.add('');
      }
    }
  }

  return outputList;
}

List<String>? distinct(List<String>? list) {
  if (list == null) {
    return []; // Return an empty list here
  }

  Set<String> distinctSet = Set<String>.from(list);
  List<String> distinctList = distinctSet.toList();

  return distinctList;
}

bool? isitsame(
  String? stringA,
  String? stringB,
) {
  if (stringA != null && stringB != null) {
    final trimmedA = stringA.trim();
    final trimmedB = stringB.trim();
    return trimmedA == trimmedB;
  }
  return null;
}

String? flfromdropdown(
  String? substation,
  String? bay,
  String? primarysecondary,
  String? object,
) {
  String l1 = '';
  String l2 = '';
  String l3 = '%';
  String l4 = '%';
  String l5 = '%';

  if (bay != null && bay.trim().isNotEmpty) {
    List<String> bayParts = bay.split('/');
    if (bayParts.length >= 1) l1 = bayParts[0];
    if (bayParts.length >= 2) l2 = bayParts[1];
    if (bayParts.length >= 3) l3 = bayParts[2];
  } else if (substation != null && substation.trim().isNotEmpty) {
    List<String> substationParts = substation.split('/');
    if (substationParts.length >= 1) l1 = substationParts[0];
    if (substationParts.length >= 2) l2 = substationParts[1];
    // l3 will be automatically set to '%'
  } else {
    return ''; // Handle the case where both bay and substation are null or empty
  }

  if (primarysecondary != null && primarysecondary.trim().isNotEmpty) {
    l4 = primarysecondary;
  } else {
    l4 = '%';
  }

  if (object != null && object.trim().isNotEmpty) {
    l5 = object;
  } else {
    l5 = '%';
  }

  return '$l1/$l2/$l3/$l4/$l5';
}

List<dynamic>? classinfofromequipmentandclassresponse(
  List<dynamic> equipmentresponse,
  String classheadercode,
  List<dynamic> characteristicinfo,
) {
  // Step 1: Filter input A based on input B
  List<dynamic> filteredEquipment = equipmentresponse
      .where((equipment) =>
          equipment["ClassHeaderCode"].trim() == classheadercode.trim())
      .toList();

  // Step 2: Add info from input C to the filtered equipment
  for (var equipment in filteredEquipment) {
    var characteristicCode = equipment["CharacteristicCode"];
    var classHeaderCode = equipment["ClassHeaderCode"].trim();

    var matchingCharacteristics = characteristicinfo
        ?.where((characteristic) =>
            characteristic["CLASS"].trim() == classHeaderCode &&
            characteristic["CharID"] == characteristicCode)
        .toList();

    if (matchingCharacteristics != null && matchingCharacteristics.isNotEmpty) {
      // Assuming there's only one matching characteristic in input C
      equipment.addAll(matchingCharacteristics[0]);
    }
  }

  // Step 3: Return the final list
  return filteredEquipment;
}

List<String>? emptylist() {
  return null;
}

List<String>? removespacelist(List<String>? inputlist) {
  if (inputlist == null) {
    return null;
  }

  List<String> cleanedList = [];
  for (String element in inputlist) {
    String cleanedElement = element.trim(); // Removes spaces at both ends
    cleanedList.add(cleanedElement);
  }
  return cleanedList;
}

List<dynamic>? filterdeletedjsonlist(List<dynamic>? jsonlist) {
  if (jsonlist == null) {
    return null;
  }

  List<Map<String, dynamic>> filteredList = [];

  for (Map<String, dynamic> jsonMap in jsonlist) {
    String bayDesc = jsonMap['BayDesc'];
    if (!bayDesc.contains('(DELETED)')) {
      filteredList.add(jsonMap);
    }
  }

  return filteredList;
}

List<dynamic>? filterduplicatevaluejson(
  List<dynamic>? original,
  String? value,
) {
  Map<String, dynamic> prefixMap = {};
  List<dynamic> result = [];

  if (original != null) {
    for (var item in original) {
      if (item is Map<String, dynamic> &&
          item[value] is String &&
          item[value].isNotEmpty) {
        String prefix = item[value];
        if (!prefixMap.containsKey(prefix)) {
          prefixMap[prefix] = true;
          result.add(item);
        }
      }
    }
  }

  return result;
}

dynamic returnsameeqart(
  List<dynamic>? object,
  String? eqart,
) {
  List<dynamic> result = [];

  if (object != null) {
    for (var item in object) {
      if (item is Map<String, dynamic> &&
          item['EQART'] is String &&
          item['EQART'].trim().toLowerCase() == eqart!.trim().toLowerCase()) {
        result.add(item);
      }
    }
  }

  return result;
}

List<dynamic> filterjsonby(
  List<dynamic> json,
  String? key,
  String? value,
) {
  if (key == null || value == null || key.isEmpty || value.isEmpty) {
    return json;
  }

  return json
      .where((item) => item[key]?.toString()?.startsWith(value) ?? false)
      .toList();
}
