import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import '/custom_code/widgets/index.dart' as custom_widgets;
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'imageuploaded_model.dart';
export 'imageuploaded_model.dart';

class ImageuploadedWidget extends StatefulWidget {
  const ImageuploadedWidget({
    Key? key,
    this.parameter1,
    this.parameter2,
  }) : super(key: key);

  final FFUploadedFile? parameter1;
  final int? parameter2;

  @override
  _ImageuploadedWidgetState createState() => _ImageuploadedWidgetState();
}

class _ImageuploadedWidgetState extends State<ImageuploadedWidget> {
  late ImageuploadedModel _model;

  @override
  void setState(VoidCallback callback) {
    super.setState(callback);
    _model.onUpdate();
  }

  @override
  void initState() {
    super.initState();
    _model = createModel(context, () => ImageuploadedModel());
  }

  @override
  void dispose() {
    _model.maybeDispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    context.watch<FFAppState>();

    return Container(
      width: MediaQuery.sizeOf(context).width * 1.0,
      height: MediaQuery.sizeOf(context).height * 1.0,
      child: custom_widgets.Imagebyte(
        width: MediaQuery.sizeOf(context).width * 1.0,
        height: MediaQuery.sizeOf(context).height * 1.0,
        image: widget.parameter1,
      ),
    );
  }
}
