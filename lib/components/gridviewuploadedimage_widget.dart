import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'gridviewuploadedimage_model.dart';
export 'gridviewuploadedimage_model.dart';

class GridviewuploadedimageWidget extends StatefulWidget {
  const GridviewuploadedimageWidget({Key? key}) : super(key: key);

  @override
  _GridviewuploadedimageWidgetState createState() =>
      _GridviewuploadedimageWidgetState();
}

class _GridviewuploadedimageWidgetState
    extends State<GridviewuploadedimageWidget> {
  late GridviewuploadedimageModel _model;

  @override
  void setState(VoidCallback callback) {
    super.setState(callback);
    _model.onUpdate();
  }

  @override
  void initState() {
    super.initState();
    _model = createModel(context, () => GridviewuploadedimageModel());
  }

  @override
  void dispose() {
    _model.maybeDispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    context.watch<FFAppState>();

    return GridView(
      padding: EdgeInsets.zero,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
        childAspectRatio: 1.0,
      ),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: Image.network(
            'https://picsum.photos/seed/241/600',
            width: 300.0,
            height: 200.0,
            fit: BoxFit.cover,
          ),
        ),
      ],
    );
  }
}
