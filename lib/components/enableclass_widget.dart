import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'enableclass_model.dart';
export 'enableclass_model.dart';

class EnableclassWidget extends StatefulWidget {
  const EnableclassWidget({Key? key}) : super(key: key);

  @override
  _EnableclassWidgetState createState() => _EnableclassWidgetState();
}

class _EnableclassWidgetState extends State<EnableclassWidget> {
  late EnableclassModel _model;

  @override
  void setState(VoidCallback callback) {
    super.setState(callback);
    _model.onUpdate();
  }

  @override
  void initState() {
    super.initState();
    _model = createModel(context, () => EnableclassModel());
  }

  @override
  void dispose() {
    _model.maybeDispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    context.watch<FFAppState>();

    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Text(
          'Disable this to remove class',
          style: FlutterFlowTheme.of(context).bodyMedium.override(
                fontFamily: 'Readex Pro',
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
              ),
        ),
        Switch.adaptive(
          value: _model.switchValue ??= true,
          onChanged: (newValue) async {
            setState(() => _model.switchValue = newValue!);
          },
          activeColor: FlutterFlowTheme.of(context).primary,
          activeTrackColor: FlutterFlowTheme.of(context).accent1,
          inactiveTrackColor: Color(0x4D57636C),
          inactiveThumbColor: FlutterFlowTheme.of(context).secondaryText,
        ),
      ],
    );
  }
}
