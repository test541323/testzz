// Automatic FlutterFlow imports
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import '/custom_code/actions/index.dart'; // Imports custom actions
import '/flutter_flow/custom_functions.dart'; // Imports custom functions
import 'package:flutter/material.dart';
// Begin custom widget code
// DO NOT REMOVE OR MODIFY THE CODE ABOVE!

class Imagebyte extends StatefulWidget {
  const Imagebyte({
    Key? key,
    this.width,
    this.height,
    this.image,
  }) : super(key: key);

  final double? width;
  final double? height;
  final FFUploadedFile? image;

  @override
  _ImagebyteState createState() => _ImagebyteState();
}

class _ImagebyteState extends State<Imagebyte> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      child: widget.image != null
          ? Image.memory(
              widget.image!.bytes!,
              width: widget.width,
              height: widget.height,
              fit: BoxFit.cover,
            )
          : Container(),
    );
  }
}
